﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace labLibs
{
    public class DoubleLinkedList<T> : ILinkedList<T>
    {
        protected class Node
        {
            public Node(T data)
            {
                this.data = data;
            }

            public T data;

            public Node prev = null;
            public Node next = null;
        }

        public void Push(T data)
        {
            if (empty)
            {
                root = new Node(data);
            }
            else
            {
                Node cur = root;
                while (cur.next != null)
                {
                    cur = cur.next;
                }

                cur.next = new Node(data);
                cur.next.prev = cur;
            }
        }

        public void Pop()
        {
            if (empty) return;

            Node cur = root;
            while (cur.next != null)
            {
                cur = cur.next;
            }

            if (cur.prev != null)
                cur.prev.next = null;
            else
                root = null;
        }

        public void InsertBefore(uint idx, T data)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node cur = root;

            while (idx != count)
            {
                ++count;
                cur = cur.next;
            }

            Node node = new Node(data);

            if (cur.prev == null)
            {
                node.next = cur;
                root = node;
            }
            else
            {
                node.prev = cur.prev;
                cur.prev.next = node;

                node.next = cur;
                cur.prev = node;
            }
        }

        public void InsertAfter(uint idx, T data)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node cur = root;

            while (cur != null && idx != count)
            {
                ++count;
                cur = cur.next;
            }

            Node node = new Node(data);

            node.next = cur.next;
            node.prev = cur;

            if (cur.next != null)
                cur.next.prev = node;

            cur.next = node;
        }

        public void Remove(uint idx)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node cur = root;

            while (idx != count)
            {
                ++count;
                cur = cur.next;
            }

            if (cur.prev != null)
            {
                cur.prev.next = cur.next;

                if (cur.next != null)
                    cur.next.prev = cur.prev;
            }
            else
            {
                if (root.next != null)
                {
                    root = root.next;
                    root.prev = null;
                }
                else
                    root = null;
            }
        }

        public T Get(uint idx)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node node = root;

            while (idx != count)
            {
                ++count;
                node = node.next;
            }

            return node.data;
        }

        public bool empty
        {
            get
            {
                return root == null;
            }
        }

        public uint size
        {
            get
            {
                uint count = 0;
                Node node = root;

                while (node != null)
                {
                    ++count;
                    node = node.next;
                }

                return count;
            }
        }

        private Node root = null;
    }
}
