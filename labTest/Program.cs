﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using labLibs;

namespace labTest
{
    class Program
    {
        
        public static void PrintIntList<T>(labLibs.ILinkedList<T> l)
        {
            if (l == null) return;

            uint count = l.size;

            Console.WriteLine("begin print:");

            for (uint i = 0; i < count; ++i)
            {
                Console.Write("{0}; ", l.Get(i));
            }

            Console.WriteLine();
            Console.WriteLine("end.");
        }

        public static void TestList(labLibs.ILinkedList<int> list1)
        {
            for (int i = 0; i < 10; ++i)
            {
                list1.Push(i);
            }

            uint s = list1.size;

            list1.InsertBefore(7, 77);
            PrintIntList(list1);

            list1.InsertBefore(9, 88);
            PrintIntList(list1);

            list1.InsertBefore(0, -10);
            PrintIntList(list1);

            list1.InsertBefore(2, 11);
            PrintIntList(list1);

            list1.InsertAfter(list1.size - 1, 1005);
            PrintIntList(list1);

            list1.InsertAfter(0, -5);
            PrintIntList(list1);

            list1.InsertAfter(8, 55);
            PrintIntList(list1);

            list1.Remove(15);

            PrintIntList(list1);

            list1.Remove(9);

            PrintIntList(list1);

            list1.Remove(0);

            PrintIntList(list1);

            list1.Remove(4);

            PrintIntList(list1);

            list1.Pop();
            list1.Pop();

            PrintIntList(list1);

            list1.Pop();
            list1.Pop();

            PrintIntList(list1);
        }

        static void TestStack()
        {
            labLibs.Stack<int> stack = new labLibs.Stack<int>();

            stack.Push(5);
            stack.Push(3);
            stack.Push(7);

            Console.WriteLine("top: {0}", stack.Top());
            
            stack.Pop();

            Console.WriteLine("top: {0}", stack.Top());

            stack.Pop();

            Console.WriteLine("top: {0}", stack.Top());

            stack.Pop();

            Console.WriteLine("is empty: {0}", stack.empty);
        }

        static void TestTree()
        {
            labLibs.BinaryTree<string, string> tree = new labLibs.BinaryTree<string, string>();

            tree.Insert("Николаев", "2894");
            tree.Insert("Нейман", "2894");
            tree.Insert("Дядченко", "2894");
            tree.Insert("Чаплыгин", "2894");
            tree.Insert("Зайцева", "2894");

            tree.Insert("Александров", "2893");
            tree.Insert("Гусев", "2893");
            tree.Insert("Гладников", "2893");

            tree.Delete("Зайцева");
            
            tree.printLKR();
            tree.printKLR();
            tree.printLRK();
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine("--- Lab 1 --------------------------------------------");
            
            Console.WriteLine("----- List ------");
            TestList(new labLibs.LinkedList<int>());
            Console.WriteLine("------------------------------------------------------");
            
            Console.WriteLine("----- Double List -----");
            TestList(new labLibs.DoubleLinkedList<int>());
            Console.WriteLine("------------------------------------------------------");
            
            Console.WriteLine("--- Lab 2 ---------------------------------------------");
            
            Console.WriteLine("----- Stack ------");
            TestStack();
            Console.WriteLine("------------------------------------------------------");
            
            Console.WriteLine("--- Lab 3 ----------------------------------------------");

            Console.WriteLine("----- Tree -----");
            TestTree();
            Console.WriteLine();
        }
    }
}
