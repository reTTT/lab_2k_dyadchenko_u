﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace labLibs
{
    public class Stack<T>
    {
        protected class Node
        {
            public Node(T data)
            {
                this.data = data;
            }

            public T data;
            public Node next = null;
        }

        public void Push(T data)
        {
            if (empty)
            {
                root = new Node(data);
            }
            else
            {
                Node node = root;

                while (node.next != null)
                {
                    node = node.next;
                }

                node.next = new Node(data);
            }
        }

        public void Pop()
        {
            if (empty) return;

            Node node = root;
            
            while (node.next != null && node.next.next != null)
            {
                node = node.next;
            }

            if (node.next == null)
                root = null;
            else
                node.next = null;
        }

        public T Top()
        {
            if (empty)
            {
                throw new IndexOutOfRangeException();
            }

            Node node = root;
            
            while (node.next != null)
            {
                node = node.next;
            }

            return node.data;
        }

        public uint size
        {
            get
            {
                uint count = 0;
                Node node = root;

                while (node != null)
                {
                    ++count;
                    node = node.next;
                }

                return count;
            }
        }

        public bool empty
        {
            get
            {
                return root == null;
            }
        }

        private Node root = null;
    }
}
