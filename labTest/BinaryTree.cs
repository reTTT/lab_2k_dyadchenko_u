﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace labLibs
{
    public class BinaryTree<Tk, Td>
    {
        public class Pair
        {
        	public Pair(Tk first, Td second)
        	{
        		this.first = first;
        		this.second = second;
        	}
        	
        	public Tk first;
        	public Td second;
        }
    	
    	protected class Node
        {
            public Node(Tk key, Td data)
            {
            	val = new Pair(key, data);
            }
            
            public Tk key
            {
            	get 
            	{
            		return val.first;
            	}
            }

            public Pair val = null;

            public Node left = null;
            public Node right = null;
        }
    	
    	public void printLKR()
    	{
    		Console.WriteLine("begin");
    		printLKR(root);
    		Console.WriteLine("end");
    		Console.WriteLine();
    	}
    	
    	void printLKR(Node node)
    	{
    		if(node == null) return;
    		
    		if(node.left != null)
    		{
    			printLKR(node.left);
    		}
    		
    		Console.WriteLine("key: {0} data: {1}", node.key, node.val.second);
    		
    		if(node.right != null)
    		{
    			printLKR(node.right);
    		}
    	}
    	
    	public void printKLR()
    	{
    		Console.WriteLine("begin");
    		printKLR(root);
    		Console.WriteLine("end");
    		Console.WriteLine();
    	}
    	
    	void printKLR(Node node)
    	{
    		if(node == null) return;
    		
    		Console.WriteLine("key: {0} data: {1}", node.key, node.val.second);
    		
    		if(node.left != null)
    		{
    			printKLR(node.left);
    		}
    		
    		if(node.right != null)
    		{
    			printKLR(node.right);
    		}
    	}
    	
    	public void printLRK()
    	{
    		Console.WriteLine("begin");
    		printLRK(root);
    		Console.WriteLine("end");
    		Console.WriteLine();
    	}
    	
    	void printLRK(Node node)
    	{
    		if(node == null) return;
    		
    		if(node.left != null)
    		{
    			printLRK(node.left);
    		}
    		
    		if(node.right != null)
    		{
    			printLRK(node.right);
    		}
    		
    		Console.WriteLine("key: {0} data: {1}", node.key, node.val.second);
    	}

        public void Insert(Tk key, Td data)
        {
            if (root == null)
            {
                root = new Node(key, data);
            }
            else
            {
                Insert(key, data, root);
            }
        }

        void Insert(Tk key, Td data, Node leaf)
        {
            if (Comparer<Tk>.Default.Compare(key, leaf.key) < 0)
            {
                if(leaf.left != null)
                    Insert(key, data, leaf.left);
                else
                {
                    Node node = new Node(key, data);
                    leaf.left = node;
                }  
            }
            else
            {
                if(leaf.right != null)
                    Insert(key, data, leaf.right);
                else
                {
                    Node node = new Node(key, data);
                    leaf.right = node;
                }
            }
        }

        public bool ContainsKey(Tk key)
        {
            return Search(key, root) != null;
        }

        Node Search(Tk key, Node leaf)
        {
            if(leaf != null)
            {
                if(Comparer<Tk>.Default.Compare(key, leaf.key) == 0)
                    return leaf;
                else if (Comparer<Tk>.Default.Compare(key, leaf.key) < 0)
                    return Search(key, leaf.left);
                else
                    return Search(key, leaf.right);
            }
            else
                return null;
        }

        public void Delete(Tk key)
        {
            Node temp = root;
            Node parent = root;

            if (temp == null)
            {
                throw new IndexOutOfRangeException("The tree is empty");
            }
            else
            {
                while (temp != null && Comparer<Tk>.Default.Compare(temp.key, key) != 0)
                {
                    parent = temp;

                    if (Comparer<Tk>.Default.Compare(temp.key, key) < 0)
                    {
                        temp = temp.right;
                    }
                    else
                    {
                        temp = temp.left;
                    }
                }
            }
 
            if (temp == null)
            {
                throw new IndexOutOfRangeException("No node present");
            }
            else if (temp == root)
            {
                if (temp.right == null && temp.left == null)
                {
                    root = null;
                }
                else if (temp.left == null)
                {
                    root = temp.right;
                }
                else if (temp.right == null)
                {
                    root = temp.left;
                }
                else
                {
                    Node temp1 = temp.right;
                   
                    while (temp1.left != null)
                    {
                        temp = temp1;
                        temp1 = temp1.left;
                    }

                    if (temp1 != temp.right)
                    {
                        temp.left = temp1.right;
                        temp1.right = root.right;
                    }

                    temp1.left = root.left;
                    root = temp1;
                }
            }
            else
            {
                if (temp.right == null && temp.left == null)
                {
                    if (parent.right == temp)
                        parent.right = null;
                    else
                        parent.left = null;
                }
                else if (temp.left == null)
                {
                    if (parent.right == temp)
                        parent.right = temp.right;
                    else
                        parent.left = temp.right;
                }
                else if (temp.right == null)
                {
                    if (parent.right == temp)
                        parent.right = temp.left;
                    else
                        parent.left = temp.left;
                }
                else
                {
                    parent = temp;
                    Node temp1 = temp.right;

                    while (temp1.left != null)
                    {
                        parent = temp1;
                        temp1 = temp1.left;
                    }

                    if (temp1 != temp.right)
                    {
                        temp.left = temp1.right;
                        temp1.right = parent.right;
                    }

                    temp1.left = parent.left;
                    parent = temp1;
                }
            }
        }

        public bool empty
        {
            get
            {
                return root == null;
            }
        }
        
        private Node root = null;
    }
}
