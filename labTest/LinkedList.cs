﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace labLibs
{
    public class LinkedList<T> : ILinkedList<T>
    {
        protected class Node
        {
            public Node(T data)
            {
                this.data = data;
            }

            public T data;
            public Node next = null;
        }
        
        public void Push(T data)
        {
            if (empty)
            {
                root = new Node(data);
            }
            else
            {
                Node old = root;
                while (old.next != null)
                {
                    old = old.next;
                }

                old.next = new Node(data);
            }
        }

        public void Pop()
        {
            if (empty) return;

            Node old = root;
            while (old.next != null && old.next.next != null)
            {
                old = old.next;
            }

            if (old.next == null)
                root = null;
            else
                old.next = null;
        }
        
        public void InsertBefore(uint idx, T data)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }
                
            uint count = 0;
            Node old = null;
            Node cur = root;

            while (idx != count)
            {
                ++count;
                old = cur;
                cur = cur.next;
            }

            Node node = new Node(data);

            if (old == null)
            {
                node.next = cur;
                root = node;
            }
            else
            {
                old.next = node;
                node.next = cur;
            }
        }

        public void InsertAfter(uint idx, T data)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node cur = root;

            while (idx != count)
            {
                ++count;
                cur = cur.next;
            }

            Node node = new Node(data);
            node.next = cur.next;
            cur.next = node;
        }

        public void Remove(uint idx)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node old = null;
            Node cur = root;

            while (idx != count)
            {
                ++count;
                old = cur;
                cur = cur.next;
            }

            if (old != null)
            {
                if (cur != null)
                    old.next = cur.next;
                else
                    old.next = null;
            }
            else
            {
                if (root.next != null)
                    root = root.next;
                else
                    root = null;
            }
        }

        public bool empty
        {
            get
            {
                return root == null;
            }
        }

        public uint size
        {
            get
            {
                uint count = 0;
                Node node = root;

                while (node != null)
                {
                    ++count;
                    node = node.next;
                }

                return count;
            }
        }

        public T Get(uint idx)
        {
            if (idx >= size)
            {
                throw new IndexOutOfRangeException();
            }

            uint count = 0;
            Node node = root;

            while (idx != count)
            {
                ++count;
                node = node.next;
            }

            return node.data;
        }

        Node root = null;
    }
}

