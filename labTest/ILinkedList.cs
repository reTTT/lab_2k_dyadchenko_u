﻿using System;

namespace labLibs
{
    public interface ILinkedList<T>
    {
        void Push(T data);
        void Pop();

        void InsertBefore(uint idx, T data);
        void InsertAfter(uint idx, T data);
        void Remove(uint idx);
        T Get(uint idx);

        uint size { get; }
        bool empty { get; }
    }
}
